package exercise;

import templates.ReactiveSources;

import java.io.IOException;
import java.util.List;

public class Exercise9 {


    public static void main(String[] args) throws IOException {

        // Use ReactiveSources.intNumbersFlux()

        // Print size of intNumbersFlux after the last item returns
        ReactiveSources.intNumbersFlux()
                .count()
                .subscribe(System.out::println);

        // Collect all items of intNumbersFlux into a single list and print it
        List<Integer> getList = ReactiveSources.intNumbersFlux()
                .toStream()
                .toList();
        System.out.println(getList);

        ReactiveSources.intNumbersFlux()
                .log("Before ")
                .collectList()
                .log("After ")
                .subscribe(System.out::println);


        // Transform to a sequence of sums of adjacent two numbers
        ReactiveSources.intNumbersFlux()
                .buffer(2)
                .map(x -> x.get(0) + x.get(1))
                .subscribe(System.out::println);

        System.out.println("Press a key to end");
        System.in.read();
    }

}