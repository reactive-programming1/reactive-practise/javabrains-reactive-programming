package exercise;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import templates.ReactiveSources;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeoutException;


public class Exercise6 {


    public static void main(String[] args) throws IOException {

        // Use ReactiveSources.unresponsiveFlux() and ReactiveSources.unresponsiveMono()

        // Get the value from the Mono into a String variable but give up after 5 seconds
        String getString = ReactiveSources.unresponsiveMono().block(Duration.ofSeconds(5));
        System.out.println("String == " + getString);

        // Get the value from unresponsiveFlux into a String list but give up after 5 seconds
        // Come back and do this when you've learnt about operators!
        List<String> getListStrings = ReactiveSources.unresponsiveFlux()
                .timeout(Duration.ofSeconds(5))
                .toStream()
                .toList();

        List<String> getListStrings2 = ReactiveSources.unresponsiveFlux()
                .timeout(Duration.ofSeconds(5))
                .doOnError(err -> System.out.println("ERROR message - " + err.getMessage()))
                .onErrorResume(e -> Flux.just("e", "r", "r", "o", "r"))
                .toStream()
                .toList();
        System.out.println(getListStrings2);

        System.out.println("Press a key to end");
        System.in.read();
    }

}