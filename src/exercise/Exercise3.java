package exercise;

import templates.ReactiveSources;

import java.io.IOException;
import java.util.List;

public class Exercise3 {

    public static void main(String[] args) throws IOException {

        // Use ReactiveSources.intNumbersFlux()

        // Get all numbers in the ReactiveSources.intNumbersFlux stream
        // into a List and print the list and its size
        List<Integer> fluxToList = ReactiveSources.intNumbersFlux().toStream().toList();
        System.out.println(fluxToList);
        System.out.println(fluxToList.size());

    }

}