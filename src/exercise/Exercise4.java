package exercise;

import templates.ReactiveSources;

import java.io.IOException;
import java.util.Optional;

public class Exercise4 {

    public static void main(String[] args) throws IOException {

        // Use ReactiveSources.intNumberMono()

        // Print the value from intNumberMono when it emits
        ReactiveSources.intNumberMono().subscribe(p-> System.out.println(p));

        // Get the value from the Mono into an integer variable
        Integer monoVariableCallingBlockOperation = ReactiveSources.intNumberMono().block();
        System.out.println(monoVariableCallingBlockOperation);

        Optional<Integer> monoVariableCallingBlockOptionalCalling = ReactiveSources.intNumberMono().blockOptional();
        System.out.println(monoVariableCallingBlockOptionalCalling);

    }

}