package exercise;

import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import templates.MySubscriber;
import templates.ReactiveSources;

import java.io.IOException;

public class Exercise5 {

    public static void main(String[] args) throws IOException {

        // Use ReactiveSources.intNumberFlux() and ReactiveSources.userMono()

        // Subscribe to a flux using the error and completion hooks
        ReactiveSources.intNumbersFlux()
                .subscribe(element -> System.out.println(element),
                        err -> System.out.println(err.getMessage()),
                        () -> {
                            System.out.println("---------------\n\tCompleted!!");
                        }
                );

        // Subscribe to a flux using an implementation of BaseSubscriber
        ReactiveSources.intNumbersFlux()
                .subscribe(new BaseSubscriber<>() {
                    @Override
                    protected void hookOnSubscribe(Subscription subscription) {
                        System.out.println("The Subscriber has been subscribed!");
                        // This is asking publisher the backpressure value that the subscriber can handle
                        request(1);
                        //This will emit values unbounded [no limit for backpressure]
                        requestUnbounded();

                    }

                    @Override
                    protected void hookOnNext(Integer value) {
                        // This request() method will call whenever Subscriber declares a backpressure value
                        request(1);
                        System.out.println("On Next -> " + value);
                    }
                });


        // This is an explicit implementation of BaseSubscriber
        ReactiveSources.intNumbersFlux()
                .subscribe(new MySubscriber<>());

        System.out.println("Press a key to end");
        System.in.read();
    }

}