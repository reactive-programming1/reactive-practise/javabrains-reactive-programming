package exercise;

import templates.ReactiveSources;

import java.io.IOException;

public class Exercise2 {
    public static void main(String[] args) throws IOException, InterruptedException {

        // Use ReactiveSources.intNumbersFlux() and ReactiveSources.userFlux()

        // Print all numbers in the ReactiveSources.intNumbersFlux stream
        ReactiveSources.intNumbersFlux()
                .subscribe(System.out::println);
        Thread.sleep(100);


        // Print all users in the ReactiveSources.userFlux stream
        ReactiveSources.userFlux()
                .subscribe(users-> System.out.println(users));


        System.out.println("Press a key to end");
        System.in.read();
    }
}