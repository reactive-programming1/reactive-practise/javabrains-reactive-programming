package templates;

import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;

public class MySubscriber<T> extends BaseSubscriber<T> {
    public void hookOnSubscribe(Subscription subscription){
        System.out.println("Subscribed!");
        requestUnbounded();
    }
    public void hookOnNext(T t){
        System.out.println("Next Item -> "+t);
    }

}
